/*
INTEGRANTES DEL GRUPO

* Juan Carlos Pinto M.
* Patrik Delgadillo Machado
*/

------------SQL JOIN---------------

use TSQL2012

/*
SELECT * FROM TABLA_A AS A
INNER JOIN TABLA_B AS B
ON A.KEY = B.KEY
*/
--------------------------------------------------------------------------
-- Example 1
-- muestra el nombre y apellido de los empleados que tienen ciertas ordenes en determinadas ciudades
-- inner join
select * from HR.Employees
select * from Sales.Orders
select * from Sales.Customers

select e.firstname, e.lastname, o.shipname, c.city
from [HR].[Employees] as e
inner join [Sales].[Orders] as o
on e.empid = o.empid
inner join [Sales].[Customers] as c
on c.custid = o.custid
order by e.firstname


-- Example 2
-- Mostar el customer id y el nombre de la compania de los clientes que tienen al menos una orden de compra
SElect * from Sales.Customers
select * from Sales.Orders

SElect  c.custid as CustomersId, c.companyname as Company_Name, o.orderid
from Sales.Customers as c
inner join Sales.Orders as o
on o.custid = c.custid


-----------------------------------------------------------
/*
SELECT * FROM TABLA_A AS A
LEFT JOIN TABLA_B AS B
ON A.KEY = B.KEY
*/
-----------------------------------------------------------
--Example 1
-- left join muestra los nombres de las ordenes que no tengan empleados asignados
select * from HR.Employees
select * from Sales.Orders

select   c.companyname
from [Sales].[Customers] as c 
left join [Sales].[Orders] as o
 on o.custid = c.custid

--Example 2
 use DBLibrary
select * from libro
select * from persona
--muestra a todas las persona que tienen o no un libro
select Persona.persona_id TablaPersona, Libro.persona_id TablaLibro
from Persona
LEFT JOIN Libro
ON Persona.persona_id=Libro.libro_id

-------------------------------------------------------------
/*
SELECT * FROM TABLA_A AS A
LEFT JOIN TABLA_B AS B
ON A.KEY = B.KEY
WHERE B.KEY IN NULL
*/
-----------------------------------------------------------------
--Example 1
--left outer join
select   c.companyname
from [Sales].[Customers] as c 
left outer join [Sales].[Orders] as o
 on o.custid = c.custid
where o.custid is null


--Example 2
--muestra a las personas que no tienen libros
select Persona.persona_id TablaPersona, Libro.persona_id TablaLibro
from Persona
LEFT JOIN Libro
ON Persona.persona_id=Libro.libro_id
where Libro.libro_id is null

-------------------------------------------------------------------
/*
SELECT * FROM TABLA_A AS A
RIGHT JOIN TABLA_B AS B
ON A.KEY = B.KEY
*/
-------------------------------------------------------------------
--Example 1
-- right join
select * from HR.Employees
select * from Sales.Orders

select   c.city
from [Sales].[Orders] as o
right  join [Sales].[Customers] as c
 on c.custid = o.custid

--Example 2
--muestra los libros que tienen o no un propietario persona
select Persona.persona_id TablaPersona, Libro.persona_id TablaLibro
from Persona
RIGHT JOIN Libro
ON Persona.persona_id=Libro.libro_id

----------------------------------------------------------------------
/*
SELECT * FROM TABLA_A AS A
RIGHT JOIN TABLA_B AS B
ON A.KEY = B.KEY
WHERE A.KEY Is NULL
*/
-----------------------------------------------------------------------
--Example 1
-- right outer join
select   c.city
from [Sales].[Orders] as o
right outer join [Sales].[Customers] as c
 on c.custid = o.custid
where o.custid is null


--Example 2
--muestra los libros que no tienen propietarios
select Persona.persona_id TablaPersona, Libro.persona_id TablaLibro
from Persona
right JOIN Libro
ON Persona.persona_id=Libro.libro_id
where Persona.persona_id is null

-------------------------------------------------------------------
/*
Select * from Table_A A
full outer join Table_B
on A.key = B.Key
*/
-------------------------------------------------------------------------
--Example 1
-- full join
select   c.city, o.orderid
from [Sales].[Orders] as o
full join [Sales].[Customers] as c
on c.custid = o.custid


 --Example 2
 --muestra todo el contenido de ambas tablas, tengan o no los registros relacion
select Persona.persona_id TablaPersona, Libro.persona_id TablaLibro
from Persona
full JOIN Libro
ON Persona.persona_id=Libro.libro_id

-----------------------------------------------------------------------
/*
Select * from Table_A A
full outer join Table_B B
on A.key = B.Key
where A.key is null
or B.key is null
*/
-----------------------------------------------------------------------
--Example 1
-- full outer join
select   c.city
from [Sales].[Orders] as o
full outer join [Sales].[Customers] as c
 on c.custid = o.custid
where o.custid is null 
or c.custid is null

--Example 2
--muestra el contenido de ambas tablas donde los registros no tienen relacion
select Persona.persona_id TablaPersona, Libro.persona_id TablaLibro
from Persona
full JOIN Libro
ON Persona.persona_id=Libro.libro_id
where Persona.persona_id is null
or Libro.libro_id is null


---------------------------------------------------------------------------

-- Cross join es el resultado de la tabla Persona por la tabla Libro
select * from libro
select * from persona
select p.persona_id TablaPersona, p.nombre,  l.persona_id TablaLibro, l.titulo
from Persona as p
cross JOIN Libro as l
order by p.persona_id