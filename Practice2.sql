create database DBLibrary

use DBLibrary

CREATE TABLE Persona(
	persona_id SMALLINT PRIMARY KEY NOT NULL identity (1,1),
	nombre VARCHAR(20) NOT NULL,
	apellido VARCHAR(20) NULL,
	genero CHAR(1) NOT NULL,
	fechaNacimiento DATE NULL,
	pais VARCHAR(20) NULL
);

CREATE TABLE Libro(
	libro_id SMALLINT NOT NULL identity (1,1),
	titulo VARCHAR(50) NOT NULL,
	precio DECIMAL(5,2) NULL,
	persona_id SMALLINT NOT NULL,
	PRIMARY KEY (libro_id),
	FOREIGN KEY(persona_id) REFERENCES Persona (persona_id)
);


select * from Persona

--1)
INSERT INTO Persona 
VALUES	('Ana', 'Almeida',	'F', '1990-02-14', 'Brasil'),
		('Bill', 'Bush',	'M', '1970-02-14', 'EEUU'),
		('Jorge', 'Gonzales', 'M', '1995-10-21', 'Bolivia'),
		('Jose', 'Perez',	'M', '1980-03-30', 'Bolivia'),
		('Juan', 'Perezio',	'M', '1990-03-30', 'Bolivia'),
		('Diego', 'Soria',	'M', '1985-03-30', 'Argentina'),
		('Nicolas', 'Mendez','M', '1980-03-30', 'Argentina');

--2)
INSERT INTO Persona 
VALUES	('Lisa', 'Sims',	'F', '1990-02-14',null),
		('Bart', 'Simpson',	'M', '1990-02-14',null),
		('Maggie', 'Simpson', 'F', '1999-10-21', null)

--3)
Insert into Persona (nombre,apellido,genero)
values ('Marge', 'Simpson', 'F');

--4)
insert into Libro 
values  ('Manual SQL','100','3'),
		('Design Pattern','150','5'),
		('Aventure 2000','60','9'),
		('Excel 2010','50','6'),
		('Word basic 2010','90','7'),
		('Access advanced','110','10'),
		('Java advanced','90','1'),
		('C# basic','70','2'),
		('Phyton','60','11'),
		('Linux','90','4');

select * from Libro

--5) Listar el nombre y apellido de la tabla �Persona� ordenados por apellido y luego por nombre.
select apellido, nombre
from Persona
order by apellido asc, nombre

--6) Listar los libros con precio mayor a 50 Bs. y que el titulo del libro empieze con la letra �A�
select * from Libro
where precio > 50 and titulo like 'A%'

--7) Listar el nombre y apellido de las personas que tengan al menos un libro cuyo precio sea mayor a 50 Bs.
select p.nombre , p.apellido, l.precio
from Persona as p
inner join Libro as l
on p.persona_id = l.persona_id
where l.precio > 50

--8) Listar el nombre y apellido de las personas que nacieron el a�o 1990
select * from persona

select nombre, apellido
from Persona
where YEAR(fechaNacimiento) = '1990'

--9) Listar los libros donde el titulo del libro contenga la letra �e� en la segunda posici�n
select * from Libro
where titulo like '_e%'

--10) Contar el numero de registros de la tabla �Persona�
select COUNT(*) Cantidad_de_Registros from Persona

--11) Listar el o los libros m�s baratos
select titulo, precio as Libro_mas_baratos from Libro
where precio between 40 and 60

--12) Listar el o los libros m�s caros      
select titulo, precio as Libro_mas_caros from Libro
where precio > 60 
order by precio

--13) Mostrar el promedio de precios de los libros
select avg(precio) Promedio from Libro

--14) Mostrar la sumatoria de los precios de los libros
select sum(precio) Sumatoria from Libro

--15) Modificar la longitud del campo titulo para que soporte hasta titulos de 300 caracteres
Alter table Libro
Alter column titulo varchar(300)

--16) Listar las personas que sean del pa�s de Colombia � de Mexico (Brasil o Argentina)
select nombre, apellido, pais from Persona
where pais = 'Brasil' or pais = 'Argentina'

--17) Listar los libros que no empiezen con la letra �T�
select * from Libro
where titulo not like 'T%'
        
select * from Libro
where titulo not like 'J%'

--18) Listar los libros que tengan un precio entre 50 a 70 Bs.
select * from Libro
where precio between 50 and 70

--19) Listar las personas AGRUPADOS por pa�s
SELECT pais, COUNT(*) as cantidad 
FROM persona GROUP BY pais

--20) Listar las personas AGRUPADOS por pa�s y genero
SELECT pais, genero, COUNT(*) as cantidad FROM persona
WHERE pais IS NOT NULL
GROUP BY pais, genero 
ORDER BY pais

--21) Listar las personas AGRUPADOS por genero, cuya cantidad sea mayor a 5 
select * from Persona

select genero, count(genero) as cantidad
from Persona
group by genero
having COUNT(genero) >= 5



--22) Listar los libros ordenados por titulo en orden descendente
select * from Libro
where titulo is not null
order by titulo desc

--23) Eliminar el libro con el titulo �SQL Server�
DELETE FROM Libro WHERE titulo='SQL Server'

DELETE FROM Libro WHERE titulo='Manual SQL'

--24) Eliminar los libros que comiencen con la letra �A�
delete from Libro
where titulo like 'A%'

--25) Eliminar las personas que no tengan libros       
select * from Persona
select * from Libro

delete Persona from Persona as p
left join Libro as l
on p.persona_id = l.persona_id
where l.persona_id is null

--26) Eliminar todos los libros

delete from Libro

truncate table Libro

--27) Modificar el nombre de la persona llamada �Marco� por �Marcos�

INSERT INTO Persona 
VALUES	('Marco', 'gonzalez',	'M', '1990-02-14', 'Espa�a')

select * from Persona

update Persona 
set nombre = 'Marcos'
where nombre = 'Marco'

--28) Modificar el precio del todos los libros que empiezen con la palabra �Calculo�  a  57 Bs.
select * from Libro

update Libro
set precio = '57'
where titulo like 'Excel%'

--29) Inventarse una consulta con GROUP BY  y HAVING  y explicar el resultado que retorna.
select * from Persona
select * from Libro

SELECT genero, COUNT(*) as cantidad 
FROM persona 
GROUP BY genero 
HAVING COUNT(Persona_id)>4

