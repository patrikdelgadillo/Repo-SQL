------------SQL JOIN---------------

use TSQL2012

/*
SELECT * FROM TABLA_A AS A
INNER JOIN TABLA_B AS B
ON A.KEY = B.KEY
*/

--Mostrar el nombre del producto y precio unitario de un producto, el nombre de la compania del proveedor y el 
--nombre de su categoria

select * from Production.Products
select * from Production.Suppliers
select * from Production.Categories

select p.productname, p.unitprice, ps.companyname, pc.categoryname
from  [Production].[Products] as p
inner join [Production].[Suppliers] as ps
on p.supplierid = ps.supplierid
inner join [Production].[Categories] as pc
on pc.categoryid = p.categoryid
order by p.unitprice


-- Mostar el customer id y el nombre de la compania de los clientes que tienen al menos una orden de compra
SElect * from Sales.Customers
select * from Sales.Orders

SElect  c.custid as CustomersId, c.companyname as Company_Name, o.orderid
from Sales.Customers as c
inner join Sales.Orders as o
on o.custid = c.custid




/*
SELECT * FROM TABLA_A AS A
LEFT JOIN TABLA_B AS B
ON A.KEY = B.KEY
*/

--muestra todos los clientes que hayan hecho un compra o no 
  
select c.custid as CodigoCliente, c.companyname as Compania, o.orderid as CodigoOrden
from Sales.Customers as C
LEFT JOIN Sales.Orders as O
ON c.custid = o.custid
order by CodigoOrden
	

/*
SELECT * FROM TABLA_A AS A
LEFT JOIN TABLA_B AS B
ON A.KEY = B.KEY
WHERE B.KEY IN NULL
*/
--Muestra a los clientes que no realizaron compras
select c.custid as CodigoCliente, c.companyname as Compania, o.orderid as CodigoOrden
from Sales.Customers as C
LEFT JOIN Sales.Orders as O
ON c.custid = o.custid
where o.orderid IS NULL;



--Muestra las ordenes que tienen al menos un cliente
select c.custid as CodigoCliente, c.companyname as Compania, o.orderid as CodigoOrden
from Sales.Customers as C
RIGHT JOIN Sales.Orders as O
ON c.custid = o.custid
order by CodigoOrden



select c.custid as CodigoCliente, c.companyname as Compania, o.orderid as CodigoOrden
from Sales.Customers as C
RIGHT OUTER JOIN Sales.Orders as O
ON c.custid = o.custid
where c.custid IS NULL;


/*
Select * from Table_A A
full outer join Table_B
on A.key = B.Key
*/

--Muestra todos los registros que tienen relacion con el campo custid
Select  c.custid as CustomersId, c.companyname as Company_Name, o.orderid
from Sales.Customers as c
full outer join Sales.Orders as o
on o.custid = c.custid


Select  c.custid as CustomersId, c.companyname as Company_Name, o.orderid
from Sales.Customers as c
full outer join Sales.Orders as o
on o.custid = c.custid
where o.custid is null
or c.custid is null




use DBLibrary
select * from libro
select * from persona
--muestra a todas las persona que tienen o no un libro
select Persona.persona_id TablaPersona, Libro.persona_id TablaLibro
from Persona
LEFT JOIN Libro
ON Persona.persona_id=Libro.libro_id

--muestra los libros que tienen o no un propietario persona
select Persona.persona_id TablaPersona, Libro.persona_id TablaLibro
from Persona
RIGHT JOIN Libro
ON Persona.persona_id=Libro.libro_id


--muestra a las personas que no tienen libros
select Persona.persona_id TablaPersona, Libro.persona_id TablaLibro
from Persona
LEFT JOIN Libro
ON Persona.persona_id=Libro.libro_id
where Libro.libro_id is null

--muestra los libros que no tienen propietarios
select Persona.persona_id TablaPersona, Libro.persona_id TablaLibro
from Persona
right JOIN Libro
ON Persona.persona_id=Libro.libro_id
where Persona.persona_id is null


--muestra todo el contenido de ambas tablas, tengan o no los registros relacion
select Persona.persona_id TablaPersona, Libro.persona_id TablaLibro
from Persona
full JOIN Libro
ON Persona.persona_id=Libro.libro_id


--muestra el contenido de ambas tablas donde los registros no tienen relacion
select Persona.persona_id TablaPersona, Libro.persona_id TablaLibro
from Persona
full JOIN Libro
ON Persona.persona_id=Libro.libro_id
where Persona.persona_id is null
or Libro.libro_id is null


select Persona.persona_id TablaPersona, Libro.persona_id TablaLibro
from Persona
full JOIN Libro
ON Persona.persona_id=Libro.libro_id


-- Cross join es el resultado de la tabla Persona por la tabla Libro
select * from libro
select * from persona
select p.persona_id TablaPersona, p.nombre,  l.persona_id TablaLibro, l.titulo
from Persona as p
cross JOIN Libro as l
order by p.persona_id
--ON Persona.persona_id=Libro.libro_id