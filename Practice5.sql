-------------------FUNCTIONS-----------------------

/*
INTEGRANTES DEL GRUPO

* Juan Carlos Pinto M.
* Patrik Delgadillo Machado
*/

--Example 1
--Funciones Escalas

select * from Libro

create function IVA(@total decimal(5,2))
returns decimal(5,2)
as 
Begin
	declare @result decimal(5,2)
	set @result = @total * 0.21
	Return (@result)
End

select titulo, precio, dbo.IVA(precio) as IVA
from Libro
-----------------------------------------------------------
--Example 2
--Funciones con valores de tabla de varias instrucciones

Select * from Persona
create function ListadoPais (@pais varchar(20))
returns @clients table
(
persona_id smallint,nombre varchar(20),
apellido varchar(20),pais varchar(20)
)
as
begin
	insert @clients select persona_id, nombre, apellido, pais
	from Persona
	where pais = @pais
	return
end

select * from ListadoPais('Bolivia')
----------------------------------------------------------------
--Example 3
--Funciones con valores de tabla en Linea

create function ListadoGenero (@genero char(1))
returns table
as return
(
select nombre, apellido,genero 
from Persona
where genero = @genero
)

select * from ListadoGenero('M')
-----------------------------------------------------------

--Question 1
--Example
CREATE TABLE personas ( identificador int NOT NULL PRIMARY KEY, 
						nombre varchar(50) NOT NULL, 
						apellido1 varchar(50) NOT NULL, )


CREATE TABLE personas2 ( identificador int PRIMARY KEY, 
						nombre varchar(50)  NULL, 
						apellido1 varchar(50)  NULL, )

INSERT INTO personas 
VALUES	('5','Ana', 'Almeida'),
		('2','Bill', 'Bush'),
		('4','Jorge', 'Gonzales')

select * from personas
