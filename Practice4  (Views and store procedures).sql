/*
INTEGRANTES DEL GRUPO

* Juan Carlos Pinto M.
* Patrik Delgadillo Machado
*/

---------------Store Procedure--------------------------------------------------
--Example 1

select * from Persona

create procedure [Lista de Personas por Pais] 
@pais varchar (20)
as 
Select nombre, apellido, genero, pais
from Persona
where pais = @pais

exec [Lista de Personas por Pais] @pais = 'Bolivia'

--------------------------------------------------------------------------------
--Example 2

create procedure InsertData 
(
@nombre varchar(20),
@apellido varchar(20),
@genero char(1),
@fechaNacimiento date,
@pais varchar (20)
)
as
insert into Persona
(nombre, apellido, genero, fechaNacimiento, pais)
values 
(@nombre, @apellido, @genero, @fechaNacimiento, @pais)

exec InsertData 'Roberto','Perez','M','1990-04-20','Peru'

select * from Persona
-----------------------------------------------------------------------------
--Example 3

create procedure buscarIdPersona
@persona_id smallint
as
select * from Persona
where persona_id=@persona_id

exec buscarIdPersona '5'

------------------------------------------------------------------------------
--Example 4

create procedure BuscarLibroxLetra
@titulo varchar(50) = '%'
as
select * from Libro
where titulo like @titulo + '%'

exec BuscarLibroxLetra 'l'

-------------------------------------------------------------------------------
--Example 5

create proc EliminarLibroxId
@libro_id smallint
as
delete Libro
where libro_id = @libro_id

exec EliminarLibroxId '9'

-------------------------------------------- Vistas
/*
CREATE VIEW V_Customer
AS SELECT First_Name, Last_Name, Country
FROM Customer;
*/
---------- ej Nro 1
CREATE VIEW List_Empleados  AS

select e.firstname, e.lastname, e.address 
from HR.Employees as e
where firstname  like '%%%a%%';
----------- ejecutando la vista
select *
from List_Empleados
where lastname like 'D%%%%%%%';

---------- ej Nro 2
CREATE VIEW Employee_Customers AS

select e.firstname, e.lastname, c.companyname 
from HR.Employees as e, Sales.Orders as o, Sales.Customers as c
where e.empid = o.empid and c.custid = o.custid
----------- ejecutando la vista
select *
from Employee_Customers
where lastname like '%%%a%%';

----------- ej Nro 3
CREATE VIEW Employee_Ordes AS

select e.firstname, e.lastname
from HR.Employees as e 
left join Sales.Orders as o
on e.empid = o.empid 
----------- ejecutando la vista
select *
from Employee_Ordes
where lastname like '%%%a%%';