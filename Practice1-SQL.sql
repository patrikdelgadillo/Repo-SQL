--1) Create DB
create database DBPrueba   

--2) delete DB
drop database DBPrueba

--2) Create Tables
use DBPrueba

create table Productos(
IdProducto int primary key not null identity(1,1),
Nombre varchar(50) not null,
Precio money not null
)

create table Clientes(
IdCliente int primary key not null identity(1,1),
Nombre varchar(50) not null,
Apellido varchar(50) not null,
Edad int not null
)

create table Ventas(
Id int primary key not null identity(1,1),
IdProducto int constraint FK_IdProducto foreign key (IdProducto) references Productos(IdProducto),
IdCliente int constraint FK_IdCliente foreign key (IdCliente) references Clientes(IdCliente),
Fecha date
)

--4) Delete Tables
Drop table Clientes

Drop table Productos

Drop table Ventas

--5)Modify Column Name Apellido by Apellidos in the table Clientes
sp_rename 'Clientes.Apellido','Apellidos','column'


--6)
truncate table Ventas


--7)Insert 5 register in each table

insert into Clientes (Nombre,Apellido,Edad) values ('Juan','Roca','30')
insert into Clientes (Nombre,Apellido,Edad) values ('Carlos','Roca','33')
insert into Clientes (Nombre,Apellido,Edad) values ('Diego','Rojas','40')
insert into Clientes (Nombre,Apellido,Edad) values ('Claudia','Perez','30')
insert into Clientes (Nombre,Apellido,Edad) values ('Sara','Connor','25')


insert into Productos (Nombre,Precio) values ('TV','300.00')
insert into Productos (Nombre,Precio) values ('Radio','50.00')
insert into Productos (Nombre,Precio) values ('Heladera','350.00')
insert into Productos (Nombre,Precio) values ('Cocina','250.00')
insert into Productos (Nombre,Precio) values ('Ventilador','100.00')


insert into Ventas (IdProducto,IdCliente) values (2,5)
insert into Ventas (IdProducto,IdCliente) values (3,3)
insert into Ventas (IdProducto,IdCliente) values (1,4)
insert into Ventas (IdProducto,IdCliente) values (4,1)
insert into Ventas (IdProducto,IdCliente) values (5,2)

--8)List all the sales
Select * from Ventas


--9)List first 10 clients
SELECT TOP 6 *  FROM Clientes


--10) List the clients, order by edad
select * from Clientes
order by Edad desc



--11)List of customers over 20
Select * from Clientes
where Edad > 20
order by Edad desc


--12) delete customers with age is 20
delete from Clientes
where Edad = 20

select * from Clientes