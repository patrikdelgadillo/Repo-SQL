create database DBLibrary

use DBLibrary

CREATE TABLE Persona(
	persona_id SMALLINT PRIMARY KEY NOT NULL identity (1,1),
	nombre VARCHAR(20) NOT NULL,
	apellido VARCHAR(20) NULL,
	genero CHAR(1) NOT NULL,
	fechaNacimiento DATE NULL,
	pais VARCHAR(20) NULL
);

CREATE TABLE Libro(
	libro_id SMALLINT NOT NULL identity (1,1),
	titulo VARCHAR(50) NOT NULL,
	precio DECIMAL(5,2) NULL,
	persona_id SMALLINT NOT NULL,
	PRIMARY KEY (libro_id),
	FOREIGN KEY(persona_id) REFERENCES Persona (persona_id)
);


select * from Persona

--1)
INSERT INTO Persona 
VALUES	('Ana', 'Almeida',	'F', '1990-02-14', 'Brasil'),
		('Bill', 'Bush',	'M', '1970-02-14', 'EEUU'),
		('Jorge', 'Gonzales', 'M', '1995-10-21', 'Bolivia'),
		('Jose', 'Perez',	'M', '1980-03-30', 'Bolivia'),
		('Juan', 'Perezio',	'M', '1990-03-30', 'Bolivia'),
		('Diego', 'Soria',	'M', '1985-03-30', 'Argentina'),
		('Nicolas', 'Mendez','M', '1980-03-30', 'Argentina');

--2)
INSERT INTO Persona 
VALUES	('Lisa', 'Sims',	'F', '1990-02-14',null),
		('Bart', 'Simpson',	'M', '1990-02-14',null),
		('Maggie', 'Simpson', 'F', '1999-10-21', null)

--3)
Insert into Persona (nombre,apellido,genero)
values ('Marge', 'Simpson', 'F');

--4)
insert into Libro 
values  ('Manual SQL','100','3'),
		('Design Pattern','150','5'),
		('Aventure 2000','60','9'),
		('Excel 2010','50','6'),
		('Word basic 2010','90','7'),
		('Access advanced','110','10'),
		('Java advanced','90','1'),
		('C# basic','70','2'),
		('Phyton','60','11'),
		('Linux','90','4');